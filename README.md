# Docker Vagrant Environment

[![CI](https://github.com/gr4unch3r/docker-vagrant-env/actions/workflows/ci.yml/badge.svg)](https://github.com/gr4unch3r/docker-vagrant-env/actions/workflows/ci.yml)

Vagrant environment inside a Docker container, with batteries included.

## Requirements

- Linux host with VirtualBox kernel modules installed
- [Docker](https://docs.docker.com/engine/install/)

## Getting Started

```
$ git clone https://github.com/gr4unch3r/docker-vagrant-env.git
$ cd docker-vagrant-env
$ make && make run
```

## License

MIT

## Author Information

gr4unch3r [at] protonmail [dot] com
