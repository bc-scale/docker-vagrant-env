FROM debian:stable-slim
LABEL maintainer "Alex Shepherd <gr4unch3r@protonmail.com>"

ARG DEBIAN_FRONTEND=noninteractive

ENV LANG=C.UTF-8
ENV VAGRANT_DEFAULT_PROVIDER=virtualbox

# Install dependencies
RUN apt-get update && \
    apt-get install -y \
    build-essential \
    python3 \
    python3-pip \
    curl \
    ca-certificates \
    gnupg \
    lsb-release \
    --no-install-recommends

# Fetch Hashicorp APT repo signing key
RUN curl -fsSL https://apt.releases.hashicorp.com/gpg | \
    gpg --dearmor -o /usr/share/keyrings/hashicorp.gpg

 # Add Hashicorp APT repo to sources
RUN echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/hashicorp.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | \
    tee /etc/apt/sources.list.d/hashicorp.list

# Install Vagrant
RUN apt-get update && \
    apt-get install -y \
    vagrant \
    --no-install-recommends

# Fetch VirtualBox APT repo signing key
RUN curl -fsSL https://www.virtualbox.org/download/oracle_vbox_2016.asc | \
    gpg --dearmor -o /usr/share/keyrings/vbox.gpg 

# Add VirtualBox APT repo to sources
RUN echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/vbox.gpg] https://download.virtualbox.org/virtualbox/debian $(lsb_release -cs) contrib" | \
    tee /etc/apt/sources.list.d/vbox.list

# Install VirtualBox as provider
RUN apt-get update && \
    apt-get install -y \
    virtualbox \
    virtualbox-ext-pack \
    --no-install-recommends

# Upgrade Pip
RUN curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py && \
    python3 get-pip.py && \
    rm get-pip.py

# Install Ansible as provisioner
RUN python3 -m pip install ansible

# Add Windows Vagrant box
WORKDIR /root/vagrant
RUN vagrant box add gr4unch3r/ubuntu-focal
COPY Vagrantfile /root/vagrant/Vagrantfile

# Cleanup image
RUN apt-get autoremove --purge -y && \
    apt-get clean -y && \
    rm -rf /var/cache/* && \
    rm -rf /var/log/* && \
    rm -rf /tmp/* && \
    rm -rf /var/tmp/*

ENTRYPOINT ["/bin/bash"]
